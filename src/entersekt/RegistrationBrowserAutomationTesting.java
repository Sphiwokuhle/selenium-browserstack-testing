package entersekt;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.virtualauthenticator.HasVirtualAuthenticator;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticator;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions.Protocol;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions.Transport;

public class RegistrationBrowserAutomationTesting {

	/**
	 * Creates a VirtualAuthenticator that uses USB as the mechanism and CTAP2 protocol
	 * @param chromeDriver
	 * @return
	 */

	public VirtualAuthenticator createUSB_CTAP2VirtualAuthenticator(WebDriver driver) {
		VirtualAuthenticatorOptions options = new VirtualAuthenticatorOptions();
		options.setTransport(Transport.USB).setHasUserVerification(true).setIsUserVerified(true);
		options.setProtocol(Protocol.CTAP2);
		VirtualAuthenticator authenticator = ((HasVirtualAuthenticator) driver).addVirtualAuthenticator(options);
		return authenticator;
	}
	
	/**
	 * Chrome 91 Registration Automation.
	 * Opens the FIDO Test page and does the registration steps using a USB CTAP2 virtualAuthenticator on the Chrome WebDriver
	 * @throws InterruptedException
	 */

	public void ChromeDriverRegistrationAutomation(String friendlyName) throws InterruptedException {
		//Automate Basic FIDO Registration on windows with Chrome
		System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
		WebDriver chromeDriver = new ChromeDriver();
		createUSB_CTAP2VirtualAuthenticator(chromeDriver);

		chromeDriver.manage().window().maximize();
		chromeDriver.manage().deleteAllCookies();
		chromeDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		chromeDriver.get("https://fido-website.debug-cluster-01.devops.entersekt.com/");
		Thread.sleep(1000);

		//add friendly name
		chromeDriver.findElement(By.id("friendlyName")).sendKeys(friendlyName);
		//start SDK
		chromeDriver.findElement(By.id("start")).click();
		Thread.sleep(1000);
		//add ready listener
		chromeDriver.findElement(By.id("readyListener")).click();
		Thread.sleep(1000);
		//add completion listener
		chromeDriver.findElement(By.id("completionListener")).click();
		Thread.sleep(1000);
		//add notification listener
		chromeDriver.findElement(By.id("addNotificationListener")).click();
		Thread.sleep(1000);
		//register FIDO Authenticator
		chromeDriver.findElement(By.id("register")).click();
		Thread.sleep(5000);
		//initiate browser interaction
		chromeDriver.findElement(By.id("initiate")).click();
	}

	/**
	 * Edge 91 Registration Automation.
	 *  Opens the FIDO Test page and does the registration steps using a USB CTAP2 virtualAuthenticator on the Edge WebDriver
	 * @throws InterruptedException
	 */

	public void EdgeDriverRegistrationAutomation(String friendlyName) throws InterruptedException {

		//Automate Basic FIDO Registration on windows with Edge 
		System.setProperty("webdriver.edge.driver", "C:\\selenium\\edgedriver_win64\\msedgedriver.exe");
		WebDriver edgeDriver = new EdgeDriver();
		createUSB_CTAP2VirtualAuthenticator(edgeDriver);

		edgeDriver.manage().window().maximize();
		edgeDriver.manage().deleteAllCookies();
		edgeDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		edgeDriver.get("https://fido-website.debug-cluster-01.devops.entersekt.com/");
		Thread.sleep(1000);


		//add friendly name
		edgeDriver.findElement(By.id("friendlyName")).sendKeys(friendlyName);
		//start SDK
		edgeDriver.findElement(By.id("start")).click();
		Thread.sleep(1000);
		//add ready listener
		edgeDriver.findElement(By.id("readyListener")).click();
		Thread.sleep(1000);
		//add completion listener
		edgeDriver.findElement(By.id("completionListener")).click();
		Thread.sleep(1000);
		//add notification listener
		edgeDriver.findElement(By.id("addNotificationListener")).click();
		Thread.sleep(1000);
		//register FIDO Authenticator
		edgeDriver.findElement(By.id("register")).click();
		Thread.sleep(5000);
		//initiate browser interaction
		edgeDriver.findElement(By.id("initiate")).click();
			
	}
}
