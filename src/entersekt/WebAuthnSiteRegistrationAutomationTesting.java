package entersekt;

//Sample test in Java to run Automate session.
import java.time.Duration;
import org.openqa.selenium.By;	
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.JavascriptExecutor;
import java.net.URL;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.virtualauthenticator.HasVirtualAuthenticator;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticator;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions.Protocol;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions.Transport;

public class WebAuthnSiteRegistrationAutomationTesting {
	
	public static final String AUTOMATE_USERNAME = "sphiwokuhleshand_eGvWrR";
	  public static final String AUTOMATE_ACCESS_KEY = "MEaxUXsrpvU81m6pkpMJ";
	  public static final String URL = "https://" + AUTOMATE_USERNAME + ":" + AUTOMATE_ACCESS_KEY + "@hub-cloud.browserstack.com/wd/hub";
	  
		public static void main(String[] args) throws Exception {
		  
	    DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("os_version", "10.0");
		caps.setCapability("device", "Samsung Galaxy S20");
		caps.setCapability("real_mobile", "true");
		caps.setCapability("browserstack.appium_version", "1.21.0");
		caps.setCapability("browserstack.local", "false");
	    caps.setCapability("name", "BStack-[Java] Webauthn.io FIDO2 Test"); // test name
		caps.setCapability("build", "BStack Build Number 6"); // CI/CD job or build name
		caps.setCapability("browserstack.console", "verbose");
		caps.setCapability("browserstack.networkLogs", "true");
		caps.setCapability("browserstack.debug", "true");

		WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));
		// driver.manage().window().fullscreen();

		
		//  Add virtualAuthenticator
		 VirtualAuthenticatorOptions options = new VirtualAuthenticatorOptions();
		 options.setTransport(Transport.BLE).setHasUserVerification(true).setIsUserVerified(true);
		 options.setProtocol(Protocol.CTAP2);

		 VirtualAuthenticator authenticator =
	     ((HasVirtualAuthenticator) driver).addVirtualAuthenticator(options);
		
		driver.get("https://webauthn.io");
		WebElement element = driver.findElement(By.id("input-email"));
		element.sendKeys("Pie");

		Select select = new Select(driver.findElement(By.id("select-attestation")));
		select.selectByIndex(1);

		// Select authType = new Select(driver.findElement(By.xpath("//*[@id=\"select-authenticator\"]")));
		// Select authType = new Select(driver.findElement(By.id("select-attestation")));
		// driver.findElement(By.id("input-email")).sendKeys("Test 1");
		// Thread.sleep(1000);
		// authType.selectByIndex(1);
		// driver.findElement(By.id("input-email")).sendKeys("Test 1");
		// driver.findElement(By.xpath("//*[@id=\"register-button\"]")).click();
		// driver.findElement(By.id("register-button")).click();
		// Thread.sleep(1000);
		// driver.findElement(By.id("login-button")).click();
		// Thread.sleep(1000);
	    
	    // Setting the status of test as 'passed' or 'failed' based on the condition; if title of the web page contains 'BrowserStack'
	    // WebDriverWait wait = new WebDriverWait(driver, 5);
		String passedStringXpath = "/html/body/div/div[1]/div/div/div/div/div[1]/h5";
		
		try {
			if (driver.findElement(By.xpath(passedStringXpath)).getText().equals("A demo of the WebAuthn specification")) {
				markTestStatus("passed", "Yaay the page contains 'You're logged in'!", driver);
			}
		} 
		catch (Exception e) {
			markTestStatus("failed", "Naay the page does not contain 'You're logged in'!", driver);
		}
		System.out.println(driver.getTitle());
	    System.out.println(driver.findElement(By.xpath(passedStringXpath)).getText());
	    driver.quit();
	  }
	  // This method accepts the status, reason and WebDriver instance and marks the test on BrowserStack
	  public static void markTestStatus(String status, String reason, WebDriver driver) {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": \""+status+"\", \"reason\": \""+reason+"\"}}");
	  }
}