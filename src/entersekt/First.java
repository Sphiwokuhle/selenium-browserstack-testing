package TEDY;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.virtualauthenticator.HasVirtualAuthenticator;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticator;
import org.openqa.selenium.support.ui.Select;

import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions.Transport;
import org.openqa.selenium.virtualauthenticator.VirtualAuthenticatorOptions.Protocol;

public class First {
	public static void main(String[] args) throws InterruptedException {
		//Use Chrome Webriver
		System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver_win32\\chromedriver.exe");
		ChromeOptions chromeoptions = new ChromeOptions();
		// chromeoptions.setAcceptInsecureCerts(true);
		// chromeoptions.addArguments("--headless");
		chromeoptions.addArguments("--ignore-certificate-errors");
		WebDriver driver = new ChromeDriver(chromeoptions);
		
		//Add virtualAuthenticator
		VirtualAuthenticatorOptions options = new VirtualAuthenticatorOptions();
		options.setTransport(Transport.USB).setHasUserVerification(true).setIsUserVerified(true);
		options.setProtocol(Protocol.CTAP2);
		
		VirtualAuthenticator authenticator =
	    ((HasVirtualAuthenticator) driver).addVirtualAuthenticator(options);
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get("https://webauthn.io");
		driver.findElement(By.id("input-email")).sendKeys("Internet");
		Thread.sleep(2000);
		Select authType = new Select(driver.findElement(By.id("select-authenticator")));
		//select cross platform which is the second
		authType.selectByIndex(1);
		driver.findElement(By.id("register-button")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("login-button")).click();
		Thread.sleep(5000);
		WebElement success = driver
				.findElement(By.xpath("/html/body/div/div[1]/div[1]/div/div/div/div/div[1]/div[1]/div/h3"));
		
		if (success.getText().equals("You're logged in!")) {
			System.out.println("Success");
		}
		System.out.println(success.getText());
	}
}
